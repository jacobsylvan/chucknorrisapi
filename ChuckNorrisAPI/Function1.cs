 using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using RestSharp;




namespace TranslatorAPI
{
    public static class ChuckNorrisAPI
    {
        [FunctionName("ChuckNorrisAPI")]
        public static async Task<HttpResponseMessage> Run([HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)]HttpRequestMessage req, TraceWriter log)
        {


            //H�mtar Kategorier fr�n API
            var client = new RestClient("https://api.chucknorris.io/jokes/categories");
            var request = new RestRequest(Method.GET);
            IRestResponse response = client.Execute(request);
            string categories = response.Content;

            //Snyggar till respons vid fel kategori
            string joke = "This category does not exist. " + "Choose between " + categories.Replace('[', ' ').Replace(']', ' ').Trim();


            //Tar in queryparam
            string category = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, "category", true) == 0)
                .Value;


            //Om kategorin existerar, tas responsen in samt konverteras till JSON
            if (categories.Contains(category))
            {


                var client2 = new RestClient("https://api.chucknorris.io/jokes/random?category=" + category);
                var request2 = new RestRequest(Method.GET);
                IRestResponse response2 = client2.Execute(request2);


                dynamic json = JsonConvert.DeserializeObject(response2.Content);
                joke = json.value;
            }
            if (category == null)
            {
                //h�mtar fr�n req body
                dynamic data = await req.Content.ReadAsAsync<object>();
                category = data?.category;
            }


            return category == null
                ? req.CreateResponse(HttpStatusCode.BadRequest, "Please pass a category on the query string or in the request body")
                : req.CreateResponse(HttpStatusCode.OK, joke.Replace('"', ' ').Trim());
        }
    }
}